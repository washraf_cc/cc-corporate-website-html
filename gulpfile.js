var gulp     = require('gulp'),
    compass  = require('gulp-for-compass');

gulp.task('copy-bootstrap', function() {
    gulp.src('node_modules/bootstrap-sass/assets/fonts/bootstrap/*')
        .pipe(gulp.dest('public/assets/fonts/bootstrap/'));

    gulp.src('node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js')
        .pipe(gulp.dest('public/assets/js/vendor/'));

    gulp.src('node_modules/bootstrap-sass/assets/stylesheets/*')
        .pipe(gulp.dest('sass/vendor/bootstrap/'));

    gulp.src('node_modules/bootstrap-sass/assets/stylesheets/bootstrap/*')
        .pipe(gulp.dest('sass/vendor/bootstrap/bootstrap'));

    gulp.src('node_modules/bootstrap-sass/assets/stylesheets/bootstrap/mixins/*')
        .pipe(gulp.dest('sass/vendor/bootstrap/bootstrap/mixins'));
});

gulp.task('copy-jquery', function() {
    gulp.src('node_modules/jquery/dist/jquery.min.js')
        .pipe(gulp.dest('public/assets/js/vendor/'));
});

gulp.task('compile-sass', function() {
    gulp.src('sass/*.scss')
        .pipe(compass({
            sassDir: 'sass',
            cssDir: 'public/assets/css',
            force: true
        }));

});

gulp.task('default', ['copy-bootstrap', 'copy-jquery', 'compile-sass']);
